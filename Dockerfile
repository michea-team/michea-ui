FROM node:20-bookworm as base
WORKDIR /app
COPY ./package.json ./yarn.lock ./
RUN yarn

FROM node:20-bookworm as builder
WORKDIR /app
COPY --from=base /app/node_modules ./node_modules
COPY . .
RUN yarn build --prod

FROM node:20-bookworm as prod
WORKDIR /app
COPY --from=base /app/package.json ./package.json
COPY --from=builder /app/node_modules ./node_modules
COPY --from=builder /app/.nuxt ./.nuxt
COPY --from=builder /app/.output ./.output
EXPOSE 3000
CMD ["yarn", "start"]