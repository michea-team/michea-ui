import type { model } from '../../types'

export default defineEventHandler(async (event) => {
  const body = await readBody(event)

  const climateModels = body.database
  const commit = body.gitlab

  try {
    await database.query('BEGIN')

    let insertvalues = ''
    const deleteids: Array<number> = []

    let add = false
    let remove = false
    climateModels.forEach((newmodel: model) => {
      if (newmodel.state === 1) {
        add = true
        const scenarios = JSON.stringify(newmodel.scenarios.scenarios)
        const searchfilters = JSON.stringify(newmodel.search_filters)
        const text = `('${searchfilters}', '${newmodel.filename}', '{"scenarios": ${scenarios}}', '${newmodel.version}'),\n`
        insertvalues += text
      } else if (newmodel.state === 2) {
        remove = true
        deleteids.push(newmodel.id)
      }
    })

    if (add) {
      const lastIndex = insertvalues.lastIndexOf(',')
      insertvalues = insertvalues.slice(0, lastIndex) + ';' + insertvalues.slice(lastIndex + 1)
      const querytext = `
            INSERT INTO climate_models (search_filters, filename, scenarios, version)
            VALUES
                ${insertvalues}`

      const query = {
        name: 'fetch-modelput',
        text: querytext
      }
      try {
        await database.query(query)
      } catch (error) {
        console.log(error)
      }
    }

    if (remove) {
      const querytext = `DELETE FROM climate_models WHERE id IN (${deleteids.toString()})`

      const query = {
        name: 'fetch-modeldelete',
        text: querytext
      }
      try {
        await database.query(query)
      } catch (error) {
        console.log(error)
      }
    }

    const gitlabresponse = await gitlab.postPipeline(commit)
    if (gitlabresponse.status !== 201) {
      throw new Error(gitlabresponse.status)
    }
    await database.query('COMMIT')
    return gitlabresponse
  } catch (e) {
    console.log(e)
    await database.query('ROLLBACK')
    return { error: 'Gitlab responded: ' + e }
  }
})
