const config = useRuntimeConfig()

const triggerPipeline = async (ciPipelineId: number) => {
  const body = new FormData()
  body.append('token', config.gitlabPipelineTriggerToken)
  body.append('ref', 'main')
  body.append('variables[RUN]', 'clean_up_job')
  body.append('variables[CURL_CI_PIPELINE_ID]', ciPipelineId.toString())

  try {
    const response = await fetch(`${config.public.gitlabUrl}/api/v4/projects/30/trigger/pipeline`, {
      body,
      method: 'POST'
    })
    const data = await response.json()
    return data
  } catch (error) {
    console.log(error)
    return { message: 'Fetch Failed' }
  }
}

export default {
  triggerPipeline
}
