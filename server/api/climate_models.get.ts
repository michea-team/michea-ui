// @ts-ignore

export default defineEventHandler(async () => {
  const query = {
    name: 'fetch-models',
    text: 'SELECT * FROM climate_models'
  }
  try {
    const { rows } = await database.query(query)
    return rows
  } catch (error) {
    console.log(error)
    return { error: `Database query error: ${error}` }
  }
})
