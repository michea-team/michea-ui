// @ts-ignore
import { H3Event } from 'h3'

export default defineEventHandler(async (event: H3Event) => {
  if (event.context.params === null) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Missing hash parameter'
    })
  }
  const query = {
    name: 'fetch-user',
    text: 'SELECT * FROM jobs WHERE ci_pipeline_id = $1',
    values: [event.context.params!.pipeline_id]
  }

  const { rows } = await database.query(query)

  return rows[0]
})
