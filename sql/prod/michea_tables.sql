-- Adminer 4.8.1 PostgreSQL 15.3 (Debian 15.3-0+deb12u1) dump

DROP TABLE IF EXISTS "climate_models";
DROP SEQUENCE IF EXISTS climate_models_id_seq;
CREATE SEQUENCE climate_models_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 9223372036854775807 CACHE 1;

CREATE TABLE "public"."climate_models" (
    "id" integer DEFAULT nextval('climate_models_id_seq') NOT NULL,
    "scenarios" json NOT NULL,
    "created_at" timestamp(6) DEFAULT now() NOT NULL,
    "version" integer DEFAULT '1' NOT NULL,
    "search_filters" jsonb NOT NULL,
    "filename" character varying(1000) NOT NULL,
    CONSTRAINT "climate_models_id" PRIMARY KEY ("id"),
    CONSTRAINT "no_duplicates_cm" UNIQUE ("search_filters", "version")
) WITH (oids = false);

DROP TABLE IF EXISTS "jobs" CASCADE;
DROP SEQUENCE IF EXISTS pipelines_id_seq;
CREATE SEQUENCE pipelines_id_seq INCREMENT 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1;

CREATE TABLE "public"."jobs" (
    "id" integer DEFAULT nextval('pipelines_id_seq') NOT NULL,
    "hash" character varying(80) NOT NULL,
    "job" character varying(30) NOT NULL,
    "ci_pipeline_id" integer NOT NULL,
    "metadata" json NOT NULL,
    "created_at" timestamp DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT "no_duplicates" UNIQUE ("hash", "job"),
    CONSTRAINT "pipelines_pkey" PRIMARY KEY ("id")
) WITH (oids = false);

DROP VIEW IF EXISTS "pipelines_overview";
CREATE TABLE "pipelines_overview" ("ci_pipeline_id" integer, "data_models" json, "jobs_executed" json, "downloaded_datasets" json, "bias_adjusted_datasets" json);

DROP TABLE IF EXISTS "pipelines_overview";
CREATE VIEW "pipelines_overview" AS SELECT p1.ci_pipeline_id,
    json_agg(DISTINCT split_part((p1.job)::text, '_'::text, 2)) AS data_models,
    ( SELECT json_object_agg(k.split, k.job) AS json_object_agg
           FROM ( SELECT DISTINCT split_part((p2.metadata ->> 'ci_job_name'::text), '_'::text, 2) AS split,
                    jsonb_agg(p2.job) AS job
                   FROM jobs p2
                  WHERE (p2.ci_pipeline_id = 725)
                  GROUP BY (split_part((p2.metadata ->> 'ci_job_name'::text), '_'::text, 2))) k) AS jobs_executed,
    ( SELECT json_object_agg(split_part((p2.job)::text, '_'::text, 2), json_build_object('hash', p2.hash, 'hex_sha', (p2.metadata -> 'hex_sha'::text), 'data_location', (p2.metadata -> 'data_location'::text), 'ci_job_name', (p2.metadata -> 'ci_job_name'::text), 'inputs', (p2.metadata -> 'inputs'::text), 'outputs', (p2.metadata -> 'outputs'::text))) AS json_object_agg
           FROM jobs p2
          WHERE ((p2.ci_pipeline_id = p1.ci_pipeline_id) AND ((p2.job)::text ~~ 'download_%'::text) AND ((p2.job)::text <> 'download_historical'::text))) AS downloaded_datasets,
    ( SELECT json_object_agg(split_part((p2.job)::text, '_'::text, 2), json_build_object('hash', p2.hash, 'hex_sha', (p2.metadata -> 'hex_sha'::text), 'data_location', (p2.metadata -> 'data_location'::text), 'ci_job_name', (p2.metadata -> 'ci_job_name'::text), 'outputs', (p2.metadata -> 'outputs'::text))) AS json_object_agg
           FROM jobs p2
          WHERE ((p2.ci_pipeline_id = p1.ci_pipeline_id) AND ((p2.job)::text ~~ 'process%'::text))) AS bias_adjusted_datasets
   FROM jobs p1
  GROUP BY p1.ci_pipeline_id;
