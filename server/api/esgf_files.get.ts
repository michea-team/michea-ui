// @ts-ignore
import { H3Event } from 'h3'

export default defineEventHandler(async (event: H3Event) => {
  const parameters = getQuery(event)

  const id = parameters.id
  const indexNode = parameters.indexNode

  const response2 = await esgf.esgfFiles(id, indexNode)
  if (response2.status === 404) {
    return []
  }

  const data1 = await response2.json()

  const files: any[] | PromiseLike<any[]> = []
  data1.response.docs.forEach((doc: { [x: string]: any }) => {
    files.push(doc.title)
  })

  return files
})
