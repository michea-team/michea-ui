// @ts-ignore
export default defineEventHandler(async (event) => {
  const parameters = getQuery(event)

  if (parameters.arg === 'p') {
    if (parameters.param1 === null) {
      throw createError({
        statusCode: 400,
        statusMessage: 'Missing jobfilter parameter'
      })
    }

    if (parameters.param2 === null) {
      throw createError({
        statusCode: 400,
        statusMessage: 'Missing rcpfilter parameter'
      })
    }

    const query = {
      name: 'fetch-jobs',
      text: 'SELECT * FROM jobs WHERE job LIKE $1 AND job LIKE $2',
      values: [parameters.param1 + '%', '%' + parameters.param2]
    }

    try {
      const { rows } = await database.query(query)
      return rows
    } catch (error) {
      console.log(error)
      return { error: `Database query error: ${error}` }
    }
  } else if (parameters.arg === 'c') {
    if (parameters.param1 === null) {
      throw createError({
        statusCode: 400,
        statusMessage: 'Missing variable parameter'
      })
    }

    if (parameters.param2 === null) {
      throw createError({
        statusCode: 400,
        statusMessage: 'Missing climate index parameter'
      })
    }

    const query = {
      name: 'fetch-climate-index',
      text: "SELECT * FROM jobs WHERE job LIKE 'calculate_index' AND metadata ->> 'CLIM_VAR' LIKE $1 AND metadata ->> 'CLIMATE_INDEX' LIKE $2",
      values: [parameters.param1 + '%', parameters.param2 + '%']
    }

    try {
      const { rows } = await database.query(query)
      return rows
    } catch (error) {
      console.log(error)
      return { error: `Database query error: ${error}` }
    }
  }
})
