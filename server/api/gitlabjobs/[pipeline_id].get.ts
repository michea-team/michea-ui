// @ts-ignore
import { H3Event } from 'h3'

export default defineEventHandler(async (event: H3Event) => {
  if (event.context.params === null) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Missing pipeline parameter'
    })
  }
  const pipelineId = event.context.params!.pipeline_id
  const response = await gitlab.pipelineJobs(pipelineId)
  if (response.message) {
    return { message: response.message }
  }

  return await response.json()
})
