// @ts-ignore
export default defineEventHandler(async () => {
  const query = {
    name: 'fetch-pipelines-overview',
    text: 'SELECT * FROM pipelines_overview'
  }

  try {
    const { rows } = await database.query(query)
    return rows
  } catch (error) {
    console.log(error)
    return { error: `Database query error: ${error}` }
  }
})
