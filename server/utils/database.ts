// @ts-ignore

import pg from 'pg'

const config = useRuntimeConfig()

const pool = new pg.Pool({
  connectionString: config.databaseUrl
})

const query = async (query: pg.QueryConfig) => {
  return await pool.query(query)
}

export default {
  query
}
