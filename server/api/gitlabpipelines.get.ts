export default defineEventHandler(async () => {
  const response = await gitlab.pipelines()

  if (response.status !== 200) {
    return { error: 'Gitlab responded: ' + response.status }
  }
  const contentType = response.headers.get('content-type')
  if (contentType !== 'application/json') {
    return { error: 'Gitlab responded with content type: ' + contentType }
  }

  return await response.json()
})
