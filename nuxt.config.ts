// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  runtimeConfig: {
    databaseUrl: 'postgres://michea:pass@postgres:5432/michea',
    gitlabPrivateToken: '',
    gitlabPipelineTriggerToken: '',
    gitlabProjectId: '30',
    esgfApiUrl: 'https://esgf-node.ipsl.upmc.fr/',
    public: {
      gitlabUrl: 'https://gitlab.com/',
      gitlabProject: 'michea-team%2Fmichea-pipeline'
    }
  },
  plugins: [
    { src: '../plugins/vue3-toastify.ts', mode: 'client' }
  ],
  devtools: { enabled: true }
})
