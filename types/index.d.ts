export type Pipeline = {
    ci_pipeline_id: number
    created_at: string
    hash: string
    id: number
    job: string
    metadata: { [key: string]: string | number | Array<string> }
}
export type sortedKeys = 'id' | 'ci_pipeline_id' | 'created_at' | 'job'
export type modelKeys = 'id' | 'created_at'
export type PipelineOv = {
    ci_pipeline_id: number
    data_models: Array<string>
    bias_adjusted_datasets: { [key: string]: { [key: string]: string } }
    jobs_executed: Object
    downloaded_datasets: { [key: string]: { [key: string]: string } }
    downloadFile: string
}
type esgfstrings = {allfiles: string, croppedfiles: string}
export type foundfile = {[key: string]: esgfstrings }
type actions = {
    action: 'update' | 'create' | 'delete',
    file_path: string,
    content?: string
}
export type commitgit = {
    branch: string,
    commit_message: string,
    actions: Array<actions>
}
export type model = {
    created_at: string
    filename: string
    search_filters: { [key: string]: string }
    id: number
    scenarios: { scenarios: Array<string>}
    state: number
    version: number
}
