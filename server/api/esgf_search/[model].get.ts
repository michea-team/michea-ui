// @ts-ignore
import { H3Event } from 'h3'

export default defineEventHandler(async (event: H3Event) => {
  if (event.context.params === null) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Missing model parameter'
    })
  }

  const model = event.context.params!.model
  const response = await esgf.esgfCordex(model)
  if (response.status === 404) {
    return []
  }

  const data = await response.json()
  if (data.response.numFound === 0) {
    return []
  }

  const files: any[] | PromiseLike<any[]> = []

  data.response.docs.forEach((dataset: { [x: string]: any }) => {
    files.push({
      id: dataset.id,
      indexNode: dataset.index_node
    })
  })
  return files
})
