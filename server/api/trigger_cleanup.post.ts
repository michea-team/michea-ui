// @ts-ignore
import { H3Event } from 'h3'

export default defineEventHandler(async (event: H3Event) => {
  const body = await readBody(event)

  if (body.ci_pipeline_id === null) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Missing ci_pipeline_id'
    })
  }

  const response = await cleanuptrigger.triggerPipeline(body.ci_pipeline_id)
  if (response.message) {
    return { error: 'Error: ' + response.message }
  }

  return response
})
