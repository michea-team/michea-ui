const config = useRuntimeConfig()

const triggerPipeline = async (Parameters) => {
  const body = new FormData()
  body.append('token', config.gitlabPipelineTriggerToken)
  body.append('ref', 'main')
  body.append('variables[RUN]', 'calculate_index_job')
  body.append('variables[EXECUTED_JOB_HASH]', Parameters.hash)
  body.append('variables[CLIM_VAR]', Parameters.clim_var)
  body.append('variables[CLIM_TRAINING_VAR]', Parameters.clim_train)
  body.append('variables[CLIMATE_INDEX]', Parameters.clim_index)

  try {
    const response = await fetch(`${config.public.gitlabUrl}/api/v4/projects/30/trigger/pipeline`, {
      body,
      method: 'POST'
    })
    const data = await response.json()
    return data
  } catch (error) {
    console.log(error)
    return { message: 'Fetch Failed' }
  }
}

export default {
  triggerPipeline
}
