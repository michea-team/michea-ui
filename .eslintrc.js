module.exports = {
  root: true,
  extends: [
    'plugin:vue/vue3-recommended',
    '@nuxtjs/eslint-config-typescript'
  ],
  rules: {
    'no-console': 'off'
  }
}
