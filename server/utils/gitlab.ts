const config = useRuntimeConfig()

const pipelineJobs = async (pipelineId: string) => {
  const encodedProject = encodeURIComponent(config.public.gitlabProject)
  try {
    return await fetch(`${config.public.gitlabUrl}/api/v4/projects/${encodedProject}/pipelines/${pipelineId}/jobs?per_page=50`, {
      headers: {
        'PRIVATE-TOKEN': config.gitlabPrivateToken
      }
    })
  } catch (error) {
    console.log(error)
    return { message: 'Fetch Failed' }
  }
}

const pipelines = async () => {
  const encodedProject = encodeURIComponent(config.public.gitlabProject)
  try {
    return await fetch(`${config.public.gitlabUrl}/api/v4/projects/${encodedProject}/pipelines?per_page=500`, {
      headers: {
        'PRIVATE-TOKEN': config.gitlabPrivateToken
      }
    })
  } catch (error) {
    console.log(error)
    return { message: 'Fetch Failed' }
  }
}

const branches = async () => {
  try {
    return await fetch(`${config.public.gitlabUrl}/api/v4/projects/${config.gitlabProjectId}/repository/branches`, {
      headers: {
        'PRIVATE-TOKEN': config.gitlabPrivateToken
      }
    })
  } catch (error) {
    console.log(error)
    return { message: 'Fetch Failed' }
  }
}

const postPipeline = async (commit: string) => {
  try {
    return await fetch(`${config.public.gitlabUrl}/api/v4/projects/${config.gitlabProjectId}/repository/commits`, {
      method: 'POST',
      headers: {
        'PRIVATE-TOKEN': config.gitlabPrivateToken,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(commit)
    })
  } catch (error) {
    console.log(error)
    return { message: 'Fetch Failed' }
  }
}

export default {
  pipelineJobs,
  pipelines,
  postPipeline,
  branches
}
