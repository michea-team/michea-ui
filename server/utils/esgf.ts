const config = useRuntimeConfig()

const esgfCordex = async (model: string) => {
  const fullmodel = model
  return await fetch(`${config.esgfApiUrl}esg-search/search?${fullmodel}&format=application%2Fsolr%2Bjson&limit=100`)
}

const esgfFiles = async (id: string, indexNode: string) => {
  return await fetch(`${config.esgfApiUrl}search_files/${id}/${indexNode}/`)
}

export default {
  esgfCordex,
  esgfFiles
}
