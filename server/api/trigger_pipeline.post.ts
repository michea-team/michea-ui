// @ts-ignore
import { H3Event } from 'h3'

export default defineEventHandler(async (event: H3Event) => {
  const body = await readBody(event)

  if (body === null) {
    throw createError({
      statusCode: 400,
      statusMessage: 'Missing Trigger Pipeline Parameters'
    })
  }
  const response = await pipelinetrigger.triggerPipeline(body)
  if (response.message) {
    return { error: 'Error: ' + response.message }
  }

  return response
})
